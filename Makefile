N_GAMES ?= 1
GAME_NAME ?= ''
RUBY_IMAGE ?= ruby:2.7.2-slim

## Run games generator. N_GAMES=1. NAME=game-name
run:
	@docker run --rm -it -v "$(PWD)":/app -w /app $(RUBY_IMAGE) ./mega-sena-random $(N_GAMES) $(GAME_NAME)

## Run ruby irb console
console:
	@docker run --rm -it -v "$(PWD)":/app -w /app $(RUBY_IMAGE) ./console

## Run bash on container
bash:
	@docker run --rm -it -v "$(PWD)":/app -w /app -e HISTFILE='/app/.bash-history' $(RUBY_IMAGE) bash


.DEFAULT_GOAL := show-help

.PHONY: show-help console
show-help:
	@echo "$$(tput bold)Commands:$$(tput sgr0)"
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		if (length($$1) == 0) { \
		    printf "\n"; \
		} \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}'




