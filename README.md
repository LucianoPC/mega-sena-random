# Mega Sena Random

## Usage

Run the following command changing the [n_games] by the amount of games that you want to create.

```bash
$ make run N_GAMES=[n_games]
```

Example: If you want to create 2 mega sena games run the following command:

```bash
$ make run N_GAMES=2
```
